# Tetris
基于 Java-swing 实现俄罗斯方块

## Preview

**普通模式**：

![Tetris](http://p6uturdzt.bkt.clouddn.com/tetris-NormalMode.PNG)

**加速模式**：

![Accel](http://p6uturdzt.bkt.clouddn.com/tetris-AccelMode.PNG)

## 设计思路

### 方块的属性

- 方块表示：
  - 由四维数组 `SHAPE[][][][]` 表示7种方块及每种方块的4种翻转状态。
  - 由`SHAPE[type][state]`可以唯一标识一个方块。
- 方块属性：
  - `type`：方块类型
  - `state`：方块翻转状态
  - `(x, y)`：方块坐标
  - `nextType`：下一方块类型
  - `nextState`：下一方块翻转状态
- 背景块：由`map[ROW][COl]`表示已固定的背景块。

### 方块的行为

**方块翻转**：通过改变方块的state属性实现翻转

```java
public void turn() {
  	int temp = state;
  	state = (state + 1) % 4;
  	// 如果旋转后不合法，还原上一状态
  	if (!check(type, state, x, y)) {
  		state = temp;
  	}
}
```

**方块下落**：通过改变方块纵坐标实现下落

```java
public void down() {
    // 如果下一个下落状态合法，则下落；不合法，则固定，清行，新建块。
  	if (check(type, state, x, y + 1)) {
  		y++;
  	} else {
  		fix(type, state, x, y);
  		clearLines();
  		createShape();
  	}
  	this.repaint();
}
```

**方块左移**：通过改变方块横坐标实现左移

```java
public void left() {
  	if (check(type, state, x - 1, y)) {
  		x--;
  	}
}
```

**方块右移**：通过改变方块横坐标实现右移

```java
public void right() {
	if (check(type, state, x + 1, y)) {
  		x++;
  	}
}
```

### 核心方法实现

**方块碰撞检测**：

```java
private boolean check(int type, int state, int x, int y) {
	for (int i = 0; i < SHAPE[type][state].length; i++) {
		for (int j = 0; j < SHAPE[type][state][0].length; j++) {
			if (SHAPE[type][state][i][j] == 1) {
				// 在坐标系中小方块坐标(x+j,y+i);在背景矩阵中小方块位置map[y+i][x+j];
				if ((x + j >= COL) || (x + j < 0) || (y + i >= ROW) || (map[y + i][x + j] == 1)) {
					return false;
				}
			}
		}
	}
	return true;
}
```

**固定当前方块到背景中**：

```java
private void fix(int type, int state, int x, int y) {
	for (int i = 0; i < SHAPE[type][state].length; i++) {
		for (int j = 0; j < SHAPE[type][state][0].length; j++) {
			// 在坐标系中小方块坐标(x+j,y+i);在背景矩阵中小方块位置map[y+i][x+j];
			if ((y + i < ROW) && (x + j >= 0) && (x + j < COL) && (map[y + i][x + j] == 0)) {
				map[y + i][x + j] = SHAPE[type][state][i][j];
				mapColor[y + i][x + j] = color[type];
			}
		}
	}
}
```

**清行加分**：

```java
private void clearLines() {
	int lines = 0;
	boolean isFull = true;
	for (int i = 0; i < map.length; i++) {
		isFull = true;
		for (int j = 0; j < map[0].length; j++) {
			if (map[i][j] == 0) {
				isFull = false;
				break;
			}
		}
		if (isFull) {
			lines++;
			for (int m = i; m > 0; m--) {
				for (int n = 0; n < map[0].length; n++) {
					map[m][n] = map[m - 1][n];
				}
			}
		}
	}
	score += lines * lines * 10;
	if (isAccelMode) {
		up();
	}
}
```

### GUI

绘制图形：重写paint()方法，使用Graphics类随意绘制。

Graphics类绘制方法如下：

```java
setColor(Color color);  // 设置画笔颜色
drawRect(int x,int y,int width,int height);  // 绘制矩形边框
fillRect(int x,int y,int width,int height);  // 填充矩形
fill3DRect(int x,int y,int width,int height, boolean raised); // 填充3D矩形。
drawLine(x1,y1,x2,y2);  // 绘制线条
setFont(Font font); // 设置字体
drawString(String text, int x, int y);  // 绘制字符串
```

## **文件目录**

com.lizich.tetris下：

```yaml
.
|——TetrisMain  # 游戏窗口，由此运行
|——TetrisCtrl  # 绘制游戏面板&游戏逻辑
```

com.lizi.tetris.singleclass下：

```yaml
.
|——Tetris # 合并了TetrisMain与TetrisCtrl
```



---



emmmmmm......不好意思，寒假作业，快到暑假了才完成。



