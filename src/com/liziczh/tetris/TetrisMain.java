package com.liziczh.tetris;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class TetrisMain extends JFrame {

	/**
	 * 版本号
	 */
	private static final long serialVersionUID = 1L;
	// 游戏面板
	private TetrisCtrl tCtrl = new TetrisCtrl();

	/**
	 * 游戏窗口初始化
	 */
	public TetrisMain() {
		// 设置标题
		this.setTitle("Lizi Tetris");
		// 设置大小
		this.setSize(tCtrl.getSize());
		// 调用方法居中
		this.setLocationRelativeTo(null);
		// 设置关闭操作：关闭窗口，程序结束运行；
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// 设置窗体大小不改变
		this.setResizable(false);
		// 添加键盘监听事件
		this.addKeyListener(keyListener);

		// 菜单栏
		JMenuBar menu = new JMenuBar();
		this.setJMenuBar(menu);
		JMenu gameMenu = new JMenu("游戏");
		JMenuItem newGameItem = gameMenu.add("新游戏");
		newGameItem.addActionListener(newGameAction);
		JMenuItem pauseItem = gameMenu.add("暂停");
		pauseItem.addActionListener(pauseAction);
		JMenuItem continueItem = gameMenu.add("继续");
		continueItem.addActionListener(continueAction);
		JMenuItem exitItem = gameMenu.add("退出");
		exitItem.addActionListener(exitAction);
		JMenu modeMenu = new JMenu("模式");
		JMenuItem normalModeItem = modeMenu.add("普通模式");
		normalModeItem.addActionListener(normalModeAction);
		JMenuItem accelModeItem = modeMenu.add("加速模式");
		accelModeItem.addActionListener(accelModeAction);
		JMenu helpMenu = new JMenu("帮助");
		JMenuItem aboutItem = helpMenu.add("关于");
		aboutItem.addActionListener(aboutAction);
		menu.add(gameMenu);
		menu.add(modeMenu);
		menu.add(helpMenu);
		// 设置窗口可见
		this.setVisible(true);
		// 添加TetrisPanel
		this.add(tCtrl);

	}

	// 键盘事件监听
	KeyListener keyListener = new KeyAdapter() {

		@Override
		public void keyPressed(KeyEvent e) {
			switch (e.getKeyCode()) {
			case KeyEvent.VK_UP:
				// ↑：旋转
				tCtrl.turn();
				tCtrl.repaint();
				break;
			case KeyEvent.VK_LEFT:
				// ←：左移
				tCtrl.left();
				tCtrl.repaint();
				break;
			case KeyEvent.VK_RIGHT:
				// →：右移
				tCtrl.right();
				tCtrl.repaint();
				break;
			case KeyEvent.VK_DOWN:
				// ↓：下移
				tCtrl.down();
				tCtrl.repaint();
				break;
			}
		}
	};

	// 新游戏
	ActionListener newGameAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			tCtrl.init();
		}
	};
	// 暂停
	ActionListener pauseAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			tCtrl.setPause();
		}
	};
	// 继续
	ActionListener continueAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			tCtrl.setContinue();
		}
	};

	// 退出
	ActionListener exitAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	};

	// 普通模式
	ActionListener normalModeAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			tCtrl.setNormal();
		}
	};

	// 加速模式
	ActionListener accelModeAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			tCtrl.setAccel();
		}
	};

	// 关于
	ActionListener aboutAction = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(tCtrl, "Tetris v1.0 from liziczh", "关于", getDefaultCloseOperation());
		}
	};

	public static void main(String[] args) {
		new TetrisMain();
	}

}
